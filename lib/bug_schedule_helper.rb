# frozen_string_literal: true

require_relative 'versioned_milestone'
require_relative 'slo_breach_helper'
require_relative 'constants/slo_targets'

module BugScheduleHelper
  UX_DEPARTMENT_REGEX = /ux department/.freeze
  PRODUCT_DESIGNER_REGEX = /product designer/.freeze
  QUALITY_DEPARTMENT_REGEX = /quality department/.freeze
  SOFTWARE_ENGINEER_IN_TEST_REGEX = /software engineer in test/.freeze

  def milestone_matches_slo_target?(milestone, severity)
    !!milestone&.start_date && milestone.start_date < slo_target_date(severity) && !milestone.expired?
  end

  def em_for_current_group
    frontend_em_for_team || backend_em_for_team
  end

  def pm_for_current_group
    pm_for_team
  end

  def set_for_current_group
    parse_role_and_specialty_to_select_group_member(QUALITY_DEPARTMENT_REGEX, SOFTWARE_ENGINEER_IN_TEST_REGEX)
  end

  def pd_for_current_group
    parse_role_and_specialty_to_select_group_member(UX_DEPARTMENT_REGEX, PRODUCT_DESIGNER_REGEX)
  end

  def parse_role_and_specialty_to_select_group_member(department_regex, role_regex)
    group_name_regex = Regexp.quote(current_group_name_with_spaces)
    parsed_from_specialty = select_team_members_by_department_specialty_role(department_regex,
                                                                             /#{group_name_regex}/,
                                                                             role_regex,
                                                                             include_ooo: true).first

    # many team members don't populate the specialty field, but instead put their group info in the role field.
    parsed_from_role = select_team_members_by_department_specialty_role(department_regex,
                                                                        nil,
                                                                        /#{role_regex.source}.*#{group_name_regex}/,
                                                                        include_ooo: true).first

    parsed_from_specialty || parsed_from_role
  end

  def mention_team_members_for_slo_target_mismatch
    @redact_confidentials = false
    members_to_ping = [em_for_current_group, pm_for_current_group, set_for_current_group, pd_for_current_group].compact
    if members_to_ping.empty?
      %(~"#{current_group_label}")
    else
      members_to_ping.join(' ')
    end
  end

  def set_milestone_with_slo_target(severity)
    milestone = VersionedMilestone.new(self).find_milestone_for_date(slo_target_date(severity))
    return unless milestone

    %(/milestone %"#{milestone.title}")
  end

  def slo_target_date(severity)
    today + slo_target(severity)
  end

  def slo_target(severity, category = "default")
    # TODO: based on # https://about.gitlab.com/handbook/engineering/quality/issue-triage
    # expand this method to include severity slo targets for special bug cateogies
    SloTargets::DEFAULT_SEVERITY_SLO_TARGETS[severity]
  end

  def today
    @today ||= Date.today
  end
end
