# frozen_string_literal: true

module Labels
  COMMUNITY_CONTRIBUTION_LABEL = 'Community contribution'

  UX_LABEL = 'UX'

  DOCUMENTATION_LABEL = 'documentation'
  TECHNICAL_WRITING_LABEL = 'Technical Writing'
  TECHNICAL_WRITING_TRIAGED_LABEL = 'tw::triaged'

  HACKATHON_LABEL = 'Hackathon'

  WORKFLOW_IN_DEV_LABEL = 'workflow::in dev'
  WORKFLOW_READY_FOR_REVIEW_LABEL = 'workflow::ready for review'

  IDLE_LABEL = 'idle'
  STALE_LABEL = 'stale'

  AUTOMATION_AUTHOR_REMINDED_LABEL = 'automation:author-reminded'
  AUTOMATION_REVIEWERS_REMINDED_LABEL = 'automation:reviewers-reminded'

  TYPE_LABELS = [
    'type::feature',
    'type::maintenance',
    'type::bug'
  ].freeze

  TYPE_IGNORE_LABEL = 'type::ignore'

  SUBTYPE_LABELS = [
    'bug::performance',
    'bug::availability',
    'bug::vulnerability',
    'bug::mobile',
    'bug::functional',
    'bug::ux',
    'feature::addition',
    'feature::enhancement',
    'feature::consolidation',
    'feature::removal',
    'maintenance::refactor',
    'maintenance::dependency',
    'maintenance::usability',
    'maintenance::test-gap',
    'maintenance::pipelines',
    'maintenance::workflow',
    'maintenance::performance',
    'maintenance::scalability'
  ].freeze

  SPECIAL_ISSUE_LABELS = [
    'support request',
    'meta',
    'triage report'
  ].freeze
end
