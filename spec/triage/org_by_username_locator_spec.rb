# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/org_by_username_locator'

RSpec.describe Triage::OrgByUsernameLocator, :clean_cache do
  let(:user_id) { 1 }
  let(:account_id) { '123abc' }
  let(:user_id_no_account) { 3 }
  let(:csv_map) do
    [
      { 'USER_ID' => '1', 'CRM_ACCOUNT_ID' => '123abc' },
      { 'USER_ID' => '2', 'CRM_ACCOUNT_ID' => '345def' },
      { 'USER_ID' => '3', 'CRM_ACCOUNT_ID' => nil },
      { 'USER_ID' => '4', 'CRM_ACCOUNT_ID' => '890xyz' }
    ]
  end

  describe '#locate_org' do
    before do
      allow(subject).to receive(:csv_map).and_return(csv_map)
    end

    context 'with valid user id' do
      context 'with related account' do
        it 'returns the correct account' do
          expect(subject.locate_org(user_id)).to eq(account_id)
        end
      end

      context 'with no related account' do
        let(:user_id) { user_id_no_account }

        it 'returns nil' do
          expect(subject.locate_org(user_id)).to eq(nil)
        end
      end

      context 'when that is not in the list' do
        let(:user_id) { 999 }

        it 'returns nil' do
          expect(subject.locate_org(user_id)).to eq(nil)
        end
      end
    end

    context 'with invalid user id' do
      context 'when user_id is nil' do
        let(:user_id) { nil }

        it 'returns nil' do
          expect(subject.locate_org(user_id)).to eq(nil)
        end
      end
    end
  end
end
