# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/label_resident_contribution'

RSpec.describe Triage::LabelResidentContribution do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        resource_author_id: resource_author_id
      }
    end
  end

  let(:resource_author_id) { 42 }
  let(:resident_contributor) { true }
  let(:detector) { instance_double('Triage::ResidentContributorDetector') }

  before do
    allow(Triage::ResidentContributorDetector).to receive(:new).and_return(detector)
    allow(detector).to receive(:resident_contributor?).with(resource_author_id).and_return(resident_contributor)
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open', 'merge_request.update']

  describe '#applicable?' do
    context 'when MR author is not a resident contributor' do
      let(:resident_contributor) { false }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment' do
      body = <<~MARKDOWN.chomp
        /label ~"#{described_class::RESIDENT_CONTRIBUOR_LABEL}"
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
