# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/team_label_inference'

RSpec.describe Triage::TeamLabelInference do
  let(:object_kind) { 'issue' }
  let(:action) { 'open' }
  let(:from_gitlab_org) { true }

  shared_examples 'nothing happens' do
    it 'nothing happens' do
      expect_no_request do
        subject.process
      end
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', %w[issue.open issue.reopen issue.update merge_request.open merge_request.reopen merge_request.update]

  describe '#applicable?' do
    include_context 'with event', 'Triage::IssuableEvent' do
      let(:object_kind) { 'merge_request' }
      let(:event_attrs) do
        {
          object_kind: object_kind,
          action: action,
          from_gitlab_org?: from_gitlab_org
        }
      end
    end

    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when Section label is changed' do
      include_context 'with event', 'Triage::IssuableEvent' do
        let(:label_names) { %w[section::dev] }
        let(:event_attrs) do
          {
            object_kind: object_kind,
            action: action,
            from_gitlab_org?: from_gitlab_org,
            label_names: label_names
          }
        end
      end

      include_examples 'event is applicable'
    end

    context 'when Stage label is changed' do
      include_context 'with event', 'Triage::IssuableEvent' do
        let(:label_names) { %w[devops::create] }
        let(:event_attrs) do
          {
            object_kind: object_kind,
            action: action,
            from_gitlab_org?: from_gitlab_org,
            label_names: label_names
          }
        end
      end

      include_examples 'event is applicable'
    end

    context 'when Group label is changed' do
      include_context 'with event', 'Triage::IssuableEvent' do
        let(:label_names) { ['group::source code'] }
        let(:event_attrs) do
          {
            object_kind: object_kind,
            action: action,
            from_gitlab_org?: from_gitlab_org,
            label_names: label_names
          }
        end
      end

      include_examples 'event is applicable'
    end
  end

  describe '#documentation' do
    include_context 'with event', 'Triage::IssuableEvent' do
      let(:action) { 'update' }
      let(:event_attrs) do
        {
          object_kind: object_kind,
          action: action,
          from_gitlab_org?: from_gitlab_org
        }
      end
    end

    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    before do
      stub_request(:get, "https://about.gitlab.com/sections.json")
        .with(
          headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'User-Agent' => 'Ruby'
          })
        .to_return(status: 200, body: read_fixture('sections.json'), headers: {})
      stub_request(:get, "https://about.gitlab.com/stages.json")
        .with(
          headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'User-Agent' => 'Ruby'
          })
        .to_return(status: 200, body: read_fixture('stages.json'), headers: {})
      stub_request(:get, "https://about.gitlab.com/groups.json")
        .with(
          headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'User-Agent' => 'Ruby'
          })
        .to_return(status: 200, body: read_fixture('groups.json'), headers: {})
    end

    context 'when no Group, Stage or Section is present' do
      let(:action) { 'update' }

      include_context 'with event', 'Triage::IssuableEvent' do
        let(:event_attrs) do
          {
            object_kind: object_kind,
            action: action,
            from_gitlab_org?: from_gitlab_org
          }
        end
      end

      it_behaves_like 'nothing happens'
    end

    context 'when only Group is present' do
      include_context 'with event', 'Triage::IssuableEvent' do
        let(:label_names) { ['group::dynamic analysis'] }
        let(:event_attrs) do
          {
            object_kind: object_kind,
            action: action,
            from_gitlab_org?: from_gitlab_org,
            label_names: label_names
          }
        end
      end

      it 'infers Stage and Section labels', :aggregate_failures do
        body = <<~MARKDOWN.chomp
          /label ~"section::sec" ~"devops::secure"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when only Stage is present' do
      let(:action) { 'reopen' }

      include_context 'with event', 'Triage::IssuableEvent' do
        let(:label_names) { %w[devops::create] }
        let(:event_attrs) do
          {
            object_kind: object_kind,
            action: action,
            from_gitlab_org?: from_gitlab_org,
            label_names: label_names
          }
        end
      end

      it 'infers Section label' do
        body = <<~MARKDOWN.chomp
          /label ~"section::dev"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when only Section is present' do
      include_context 'with event', 'Triage::IssuableEvent' do
        let(:label_names) { %w[section::ops] }
        let(:event_attrs) do
          {
            object_kind: object_kind,
            action: action,
            from_gitlab_org?: from_gitlab_org,
            label_names: label_names
          }
        end
      end

      it_behaves_like 'nothing happens'
    end

    context 'when only Stage and Group are present' do
      context 'when the Stage and Group match' do
        let(:object_kind) { 'merge_request' }
        let(:action) { 'update' }

        include_context 'with event', 'Triage::IssuableEvent' do
          let(:label_names) { %w[devops::create group::editor] }
          let(:event_attrs) do
            {
              object_kind: object_kind,
              action: action,
              from_gitlab_org?: from_gitlab_org,
              label_names: label_names
            }
          end
        end

        it 'only infers the Section label' do
          body = <<~MARKDOWN.chomp
            /label ~"section::dev"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end
      end

      context 'when the Stage and Group do not match' do
        let(:object_kind) { 'merge_request' }
        let(:action) { 'reopen' }

        include_context 'with event', 'Triage::IssuableEvent' do
          let(:label_names) { %w[devops::configure group::editor] }
          let(:event_attrs) do
            {
              object_kind: object_kind,
              action: action,
              from_gitlab_org?: from_gitlab_org,
              label_names: label_names
            }
          end
        end

        it 'infers the Stage from the Group and infers the Section from the Group' do
          body = <<~MARKDOWN.chomp
            /label ~"section::dev" ~"devops::create"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end
      end
    end

    context 'when Section, Stage and Group and all are matching' do
      include_context 'with event', 'Triage::IssuableEvent' do
        let(:label_names) { ['section::sec', 'devops::secure', 'group::dynamic analysis'] }
        let(:event_attrs) do
          {
            object_kind: object_kind,
            action: action,
            from_gitlab_org?: from_gitlab_org,
            label_names: label_names
          }
        end
      end

      it_behaves_like 'nothing happens'
    end

    context 'when Section, Stage and Group are present but Stage is mismatched from Section and Group' do
      include_context 'with event', 'Triage::IssuableEvent' do
        let(:label_names) { ['section::sec', 'devops::create', 'group::dynamic analysis'] }
        let(:event_attrs) do
          {
            object_kind: object_kind,
            action: action,
            from_gitlab_org?: from_gitlab_org,
            label_names: label_names
          }
        end
      end

      it 'sets the appropriate Stage label to align with Section and Group' do
        body = <<~MARKDOWN.chomp
          /label ~"devops::secure"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when Section, Stage and Group are present but Group is mismatching with Section and Stage' do
      include_context 'with event', 'Triage::IssuableEvent' do
        let(:label_names) { ['section::ops', 'devops::create', 'group::project management'] }
        let(:event_attrs) do
          {
            object_kind: object_kind,
            action: action,
            from_gitlab_org?: from_gitlab_org,
            label_names: label_names
          }
        end
      end

      it 'updates the Stage and Section to match the Group' do
        body = <<~MARKDOWN.chomp
          /label ~"devops::plan" ~"section::dev"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when only Section and Group are present' do
      include_context 'with event', 'Triage::IssuableEvent' do
        let(:label_names) { ['section::sec', 'group::dynamic analysis'] }
        let(:event_attrs) do
          {
            object_kind: object_kind,
            action: action,
            from_gitlab_org?: from_gitlab_org,
            label_names: label_names
          }
        end
      end

      it 'infers the Stage from Group' do
        body = <<~MARKDOWN.chomp
          /label ~"devops::secure"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when only Section and Stage are present' do
      context 'when they are matching' do
        include_context 'with event', 'Triage::IssuableEvent' do
          let(:label_names) { %w[section::sec devops::secure] }
          let(:event_attrs) do
            {
              object_kind: object_kind,
              action: action,
              from_gitlab_org?: from_gitlab_org,
              label_names: label_names
            }
          end
        end

        it_behaves_like 'nothing happens'
      end

      context 'when they are mismatching' do
        include_context 'with event', 'Triage::IssuableEvent' do
          let(:label_names) { %w[section::dev devops::secure] }
          let(:event_attrs) do
            {
              object_kind: object_kind,
              action: action,
              from_gitlab_org?: from_gitlab_org,
              label_names: label_names
            }
          end
        end

        it 'the Stage takes precedence and updates the Section' do
          body = <<~MARKDOWN.chomp
            /label ~"section::sec"
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end
      end
    end
  end
end
