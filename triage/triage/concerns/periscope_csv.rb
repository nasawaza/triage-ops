# frozen_string_literal: true

require 'csv'

require_relative '../../triage'

module Triage
  module PeriscopeCsv
    CsvUrlMissingError = Class.new(StandardError)
    CsvUrlInvalidError = Class.new(StandardError)

    CSV_URL_VALIDATION_HOST = 'app.periscopedata.com'
    CSV_URL_VALIDATION_STRING = 'api/gitlab'
    CSV_CACHE_EXPIRY = 14400

    def csv_map
      ::Triage.cache.get_or_set(self.class.name, expires_in: CSV_CACHE_EXPIRY) do
        retrieve_csv.map(&:to_hash)
      end
    end

    private

    def retrieve_csv
      body = HTTParty.get(csv_url)
      CSV.parse(body.parsed_response, headers: :first_row)
    end

    def csv_url
      ENV.fetch(self.class.const_get('CSV_URL_VAR')).tap do |url|
        validate_url!(url)
      end
    end

    def validate_url!(url)
      uri = begin
        URI.parse(url)
      rescue URI::InvalidURIError
        raise CsvUrlMissingError, "CSV Url is missing. The \"#{url}\" Env variable needs to be declared"
      end

      raise CsvUrlInvalidError, 'CSV Url is invalid: CSV Url is for the wrong host' unless uri.host == CSV_URL_VALIDATION_HOST
      raise CsvUrlInvalidError, 'CSV Url is invalid: CSV Url looks incorrect' unless url.include?(CSV_URL_VALIDATION_STRING)
    end
  end
end
