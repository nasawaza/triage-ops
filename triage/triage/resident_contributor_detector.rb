# frozen_string_literal: true

require_relative 'concerns/periscope_csv'

module Triage
  class ResidentContributorDetector
    include PeriscopeCsv

    CSV_URL_VAR = 'RESIDENT_CONTRIBUTORS_CSV_URL'
    AUTHOR_ID_HEADER_NAME = 'AUTHOR_ID'

    def resident_contributor?(user_id)
      csv_map.any? { |row| row[AUTHOR_ID_HEADER_NAME] == user_id.to_s }
    end
  end
end
