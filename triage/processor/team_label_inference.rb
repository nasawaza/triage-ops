# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../../lib/www_gitlab_com'

module Triage
  class TeamLabelInference < Processor
    react_to 'issue.open', 'issue.reopen', 'issue.update',
             'merge_request.open', 'merge_request.reopen', 'merge_request.update'

    # Applicable when a section::, devops:: or group:: label is present/changed
    # @return [Boolean] true when
    def applicable?
      event.from_gitlab_org? &&
        event.label_names.any? { |l| l.match?(/section::.+|devops::.+|group::.+/) }
    end

    def process
      @labels = event.label_names.clone

      @incompatible_labels = {}
      @new_labels = {}

      @sections ||= WwwGitLabCom.sections
      @stages ||= WwwGitLabCom.stages
      @groups ||= WwwGitLabCom.groups

      validate_labels

      add_comment(comment, append_source_link: false) if @new_labels.any? || @incompatible_labels.any?
    end

    def documentation
      <<~TEXT
        Section, Stage and Group labels must align.

        Group will always take priority over Stage and Section labels. This will allow for the least amount of
        changes. I.e., when a Group label is added/updated, Stage and Section labels are updated.

        - When no Group Stage or Section labels exist: Do nothing
        - When only a Section label is present: Do nothing
        - When only a Group label is present: Infer Stage and Section label based on the Group
        - When only a Stage label is present: Infer Section label based on the Stage
        - When only Stage and Group labels are present
          - If the Stage and Group labels match: Only infer the Section label
          - If the Stage and Group labels mismatch: Change the Stage and Section labels based on the Group
        - When only Section and Group labels are present
          - If the Section and Group labels match: Only infer the Stage label
          - If the Section and Group labels mismatch: Change the Section and Stage labels based on the Group
        - When all Section, Stage and Group labels are present
          - If the Section does not match the Group: Change the Section label based on the Group
          - If the Stage does not match the Group: Change the Stage label based on the Group

        Examples:
          Good:
            "section::dev" "devops::create" "group::source code"
            "section::dev" "devops::plan" "group::editor"

          Bad:
            "section::ops" "devops::create" "group::source code" #=> Problem: Incorrect Section; Action: change section::ops to section::dev
            "devops::create" "group::source code" #=> Problem: No Section; Action: add section::dev
            "section::ops" #=> Problem: Missing Stage and Group labels; Action: Leave a note for adding Stage and Group labels
            "section::dev" "devops::create" #=> Problem: Missing Group label; Action: Do nothing.
            "devops::plan" "group::editor" => Problem: No Section, Incorrect Stage; Action: add section::dev, change devops::plan to devops::create

        Note: This processor depends on a valid Enterprise License to support Scoped Labels.  If no EE
              license is present, you may receive two identical comments during an update.
      TEXT
    end

    private

    def validate_labels
      validate_group_label if group_label
      validate_devops_label if devops_label
      validate_section_label if section_label
    end

    # Validates the ~group:: label
    def validate_group_label
      infer_section_from_group unless section_label
      infer_devops_from_group unless devops_label

      # validate that the existing devops label applies to the group. If not, change it
      @new_labels[expected_devops_label] = group_label if devops_label != expected_devops_label
    end

    # Validates the ~devops:: label
    def validate_devops_label
      infer_section_from_devops if section_label != expected_section_label
    end

    # Validates the ~section:: label
    def validate_section_label
      return unless devops && group

      @new_labels[expected_section_label] = group_label if section_label != expected_section_label
    end

    # Given a Group label, infer the ~devops:: label
    def infer_devops_from_group
      @new_labels[expected_devops_label] = group_label
      @labels << expected_devops_label
    end

    # Given a Group label, infer the ~section:: label
    def infer_section_from_group
      @new_labels[expected_section_label] = group_label
      @labels << expected_section_label
    end

    def infer_section_from_devops
      @new_labels[expected_section_label] = devops_label
    end

    # @return [String,nil] the full name of the group label
    # @see #group to return the label name (without the scope)
    def group_label
      @labels.find { |l| l.start_with?('group::') }
    end

    # @return [String,nil] the full name of the devops label
    # @see #devops to return the label name (without the scope)
    def devops_label
      @labels.find { |l| l.start_with?('devops::') }
    end

    # @return [String,nil] the full name of the section label
    def section_label
      @labels.find { |l| l.start_with?('section::') }
    end

    # @return [String,nil] the name of the group label
    # @note this returns an underscored name so as to match the json
    # @see #group_label to return the full label name
    def group
      return unless group_label

      group_label&.split('::')&.last&.tr(' ', '_')
    end

    # @return [String,nil] the name of the devops label
    # @note this returns an underscored name so as to match the json
    # @see #devops_label to return the full label name
    def devops
      return unless devops_label

      devops_label&.split('::')&.last&.tr(' ', '_')
    end

    # Based on the ~group:: label, determine what the appropriate ~devops:: label should be
    def expected_devops_label
      @stages[@groups[group]['stage']]['label']
    end

    # Based on the ~devops:: label, determine what the appropriate ~section:: label should be
    # @note Group takes precedence over the Stage label
    def expected_section_label
      return "section::#{@groups[group]['section']}" if group

      "section::#{@stages[devops]['section']}"
    end

    # Return a GitLab-formatted label
    # @return [String] GitLab-formatted label markdown
    # @example
    #   markdown_label('test') #=> %(~"test")
    #   markdown_label('scoped::label') #=> %(~"scoped::label")
    def markdown_label(label)
      %(~"#{label}")
    end

    # Markdown comment to be left on the Issuable detailing what changes were made
    # and why
    # @return [String] GitLab-friendly Markdown
    def comment
      quick_action_labels = @new_labels.keys.map { |label| markdown_label(label) }

      <<~MARKDOWN.chomp
        /label #{quick_action_labels.join(' ')}
      MARKDOWN
    end
  end
end
