# frozen_string_literal: true

require_relative 'community_processor'

require_relative '../../triage'
require_relative '../../triage/resident_contributor_detector'

module Triage
  class LabelResidentContribution < CommunityProcessor
    RESIDENT_CONTRIBUOR_LABEL = 'Resident Contributor'

    react_to 'merge_request.open', 'merge_request.update'

    def applicable?
      contribution_from_resident_contributor?
    end

    def process
      label_resident_contribution
    end

    def documentation
      <<~TEXT
        This processor automatically adds the `Resident Contributor` label to merge requests
        opened by a resident contributor.
      TEXT
    end

    private

    def contribution_from_resident_contributor?
      ResidentContributorDetector.new.resident_contributor?(event.resource_author_id)
    end

    def label_resident_contribution
      add_comment(%(/label ~"#{RESIDENT_CONTRIBUOR_LABEL}"), append_source_link: false)
    end
  end
end
