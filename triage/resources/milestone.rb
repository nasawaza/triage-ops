# frozen_string_literal: true

module Triage
  class Milestone
    attr_reader :attributes

    def initialize(attributes)
      @attributes = attributes
    end

    def in_progress?
      started? && !expired?
    end

    def starts_within_the_next_days?(days)
      range = today..(today + days)
      !start_date.nil? && range.cover?(start_date)
    end

    private

    def start_date
      @start_date ||= attributes['start_date'] && Date.parse(attributes['start_date'])
    end

    def due_date
      @due_date ||= attributes['due_date'] && Date.parse(attributes['due_date'])
    end

    def started?
      !start_date.nil? && start_date <= today
    end

    def expired?
      !due_date.nil? && due_date < today
    end

    def today
      @today ||= Date.today
    end
  end
end
