#!/usr/bin/env ruby
# frozen_string_literal: true

require 'active_support/core_ext/string/inflections'
require 'erb'

HANDLER_FILE_PATH = File.expand_path("../triage/triage/handler.rb", __dir__)
HANDLER_SPEC_FILE_PATH = File.expand_path("../spec/triage/handler_spec.rb", __dir__)

Processor = Struct.new(:name, :react_to, keyword_init: true) do
  def template
    File.expand_path("../triage/templates/processor.rb.erb", __dir__)
  end

  def spec_template
    File.expand_path("../triage/templates/processor_spec.rb.erb", __dir__)
  end

  def class_name
    name.classify
  end

  def file_name(with_extension: true)
    name.underscore + (with_extension ? ".rb" : '')
  end

  def spec_file_name
    "#{name.underscore}_spec.rb"
  end

  def path
    File.expand_path("../triage/processor/#{file_name}", __dir__)
  end

  def spec_path
    File.expand_path("../spec/processor/#{spec_file_name}", __dir__)
  end

  def react_to
    (self[:react_to] || ['issue.*', 'merge_request.*']).map { |event| "'#{event}'" }.join(', ')
  end
end

Option = Struct.new(:name, :react_to, keyword_init: true) do
  def self.parse(argv)
    require 'optparse'

    argv << '--help' if argv.empty?
    options = new

    OptionParser.new do |parser|
      parser.banner = "Usage: #{$PROGRAM_NAME} [options]\n\n"

      parser.on('-n', '--name PROCESSOR_NAME', String, 'The processor name') do |value|
        options.name = value
      end

      parser.on('-r', '--react-to [EVENTS_LIST]', Array, 'A list of events to react to') do |value|
        options.react_to = value
      end

      parser.on('-h', '--help', 'Print help message') do
        $stdout.puts parser
        exit
      end
    end.parse!(argv)

    options
  end
end

def find_line(lines, searched_line)
  found_line = lines.index("#{searched_line}\n")

  raise "Couldn't find line `#{searched_line}`!" unless found_line

  found_line
end

def update_handler_file(handler_file, processor)
  handler_file_content = File.read(handler_file)
  return if handler_file_content.include?(processor.class_name)

  handler_file_lines = handler_file_content.lines

  require_listener_line = find_line(handler_file_lines, "require_relative 'listener'")
  handler_file_lines.insert(require_listener_line, "require_relative '../processor/#{processor.file_name(with_extension: false)}'\n")

  default_processors_array_line = find_line(handler_file_lines, "    DEFAULT_PROCESSORS = [")
  handler_file_lines.insert(default_processors_array_line + 1, "      #{processor.class_name},\n")

  File.write(handler_file, handler_file_lines.join)
end

def update_handler_spec_file(handler_spec_file, processor)
  handler_spec_file_content = File.read(handler_spec_file)
  return if handler_spec_file_content.include?(processor.class_name)

  handler_spec_file_lines = handler_spec_file_content.lines

  example_line = find_line(handler_spec_file_lines, "    it 'includes all processor implementations' do")
  handler_spec_file_lines.insert(example_line + 2, "        Triage::#{processor.class_name},\n")

  File.write(handler_spec_file, handler_spec_file_lines.join)
end

options = Option.parse(ARGV)

processor = Processor.new(options.to_h)

puts "- Generating #{processor.path}..."
erb = ERB.new(File.read(processor.template), trim_mode: '-')
File.write(processor.path, erb.result_with_hash(processor: processor))

puts "- Generating #{processor.spec_path}..."
erb = ERB.new(File.read(processor.spec_template), trim_mode: '-')
File.write(processor.spec_path, erb.result_with_hash(processor: processor))

update_handler_file(HANDLER_FILE_PATH, processor)
update_handler_spec_file(HANDLER_SPEC_FILE_PATH, processor)
